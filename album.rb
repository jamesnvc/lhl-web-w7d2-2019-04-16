class Album < ActiveRecord::Base
  has_many :tracks
  has_many :playlists, through: :tracks
end

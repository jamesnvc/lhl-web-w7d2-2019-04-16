require 'active_record'
require 'faker'
require 'pry'

require_relative './album'
require_relative './track'
require_relative './playlist'

# makes it so activerecord prints out the sql queries it's doing
ActiveRecord::Base.logger = Logger.new(STDOUT)

# connect to our database
ActiveRecord::Base.establish_connection(
  adapter:  'postgresql',
  host:     'localhost',
  username: 'tester', # this will depend on your setup
  password: 'tester', # this will depend on your setup
  database: 'lhl_2019_04_16_093312'
)

ActiveRecord::Schema.define do
  create_table :albums, force: true do |t|
    t.string :title, null: false
    t.integer :year, null: false
  end

  create_table :tracks, force: true do |t|
    t.string :title, null: false
    t.string :artist, null: false
    t.bigint :album_id
  end

  create_table :playlists, force: true do |t|
    t.string :name, null: false
  end

  create_join_table :tracks, :playlists, force: true do |t|
    t.bigint :track_id, null: false
    t.bigint :playlist_id, null: false
  end
end

# Album.create(...)
# a = Album.new(...); a.save

# Album.create!(...)
# a = Album.new(...); a.save!

5.times do
  album = Album.create!(title: Faker::Music.album,
                        year: rand(1900..2020))
  rand(5..10).times do
    # Track.create!(title: Faker::Music::Phish.song,
    #               artist: Faker::Music::RockBand.name,
    #               album_id: album.id)
    album.tracks.create!(title: Faker::Music::Phish.song,
                         artist: Faker::Music::RockBand.name)
  end
end

all_tracks = Track.all
3.times do
  p = Playlist.create!(name: Faker::TvShows::Buffy.quotes)
  rand(5..10).times do
    p.tracks << all_tracks.sample
    # same thing:
    # all_tracks.sample.playlists << p
  end
end

binding.pry
